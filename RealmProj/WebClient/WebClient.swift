//
//  WebClient.swift
//  RealmProj
//
//  Created by Yaroslav Georgievich on 16.07.2019.
//  Copyright © 2019 Pavliuk. All rights reserved.
//

import Foundation

class WebClient {
    
    func fetchDataFromApi(url: URLRequest, completion: @escaping (Data?, Error?) -> Void) {
        let session = URLSession.shared
        
        let dataTask = session.dataTask(with: url) { (data, response, error) in
            guard error == nil else {
                completion(nil, error)
                return
            }
            guard let data = data else { return }
            
            completion(data, error)
            
        }
        dataTask.resume()
    }
}
