//
//  ViewController.swift
//  RealmProj
//
//  Created by Yaroslav Georgievich on 16.07.2019.
//  Copyright © 2019 Pavliuk. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func addAlert() {
        let alert = UIAlertController(title: "Clear", message: "Are you sure you want to clear list", preferredStyle: .actionSheet)
        let action1 = UIAlertAction(title: "Yes", style: .default) { (action) in
            //Чистим список
            RealmManager.sharedInstatnce.clearData()
        }
        let action2 = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            print("2")
        }
        
        alert.addAction(action1)
        alert.addAction(action2)
        
        present(alert, animated: true, completion: nil)
    }
    
    func presentAllPostVC() {
        let allPostsVC = storyboard?.instantiateViewController(withIdentifier: "AllPostViewController") as! AllPostViewController
        self.navigationController?.pushViewController(allPostsVC, animated: true)
//        self.present(allPostsVC, animated: true)
    }
    
    func pushAddPostVC() {
        let addPostsVC = storyboard?.instantiateViewController(withIdentifier: "AddPostViewController") as! AddPostViewController
        self.navigationController?.pushViewController(addPostsVC, animated: true)
    }

    @IBAction func allPostsButton(_ sender: Any) {
        presentAllPostVC()
    }
    @IBAction func deleteButton(_ sender: Any) {
        addAlert()
    }
    @IBAction func addPostButton(_ sender: Any) {
        pushAddPostVC()
    }
    
    
}

