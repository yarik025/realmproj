//
//  RealmManager.swift
//  RealmProj
//
//  Created by Yaroslav Georgievich on 16.07.2019.
//  Copyright © 2019 Pavliuk. All rights reserved.
//

import Foundation
import RealmSwift

class RealmManager {
    
    static let sharedInstatnce: RealmManager = {
        let instance = RealmManager()
        return instance
    }()
    
    let realm = try! Realm()
    
//    func setData() -> [RealmModel] {
//
//        deleteAll(array: Array(realm.objects(RealmModel.self)))
//
//        let realmObjc = RealmModel()
//        realmObjc.name = "first"
//        realmObjc.surname = "surname1"
//
//        writeObjc(realmObjc: realmObjc)
//
//        let realmObjc1 = RealmModel()
//        realmObjc1.name = "second"
//        realmObjc1.surname = "surname2"
//
//        writeObjc(realmObjc: realmObjc1)
//
//        let arrayRealmObjc = Array(realm.objects(RealmModel.self))
//        
//        return arrayRealmObjc
//    }
    func clearData() {
        deleteAll(array: Array(realm.objects(RealmUser.self)))
    }
    
    func fetchData() -> [RealmUser] {
        
        let arrayRealmObjc = Array(realm.objects(RealmUser.self))
        
        return arrayRealmObjc
    }
    
    func setData(model: RealmUser) {
        let realmObjc = model
        writeObjc(realmObjc: realmObjc)
    }
    
    func appendObjc(realmObjc: RealmUser) {
        
    }
    
    func writeObjc(realmObjc: RealmUser) {
        do {
            try realm.write {
                realm.add(realmObjc)
            }
        } catch {
            print("Ooops, error")
        }
    }
    
    func deleteAll(array: [RealmUser]) {
        if !array.isEmpty {
            try! realm.write {
                realm.delete(array)
            }
        }
    }
}
