//
//  RealmModel.swift
//  RealmProj
//
//  Created by Yaroslav Georgievich on 16.07.2019.
//  Copyright © 2019 Pavliuk. All rights reserved.
//

import Foundation
import RealmSwift

class RealmModel: Object {
    @objc dynamic var name = ""
    @objc dynamic var surname = ""
    @objc dynamic var age = Int()
    @objc dynamic var sex = ""
    @objc dynamic var cityOfBirthday = ""
}
