//
//  AllPostCell.swift
//  RealmProj
//
//  Created by Yaroslav Georgievich on 16.07.2019.
//  Copyright © 2019 Pavliuk. All rights reserved.
//

import UIKit

class AllPostCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var surnameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func update(realm: RealmUser) {
        nameLabel.text = realm.name
        surnameLabel.text = realm.surname
    }
    
}
