//
//  AllPostViewController.swift
//  RealmProj
//
//  Created by Yaroslav Georgievich on 16.07.2019.
//  Copyright © 2019 Pavliuk. All rights reserved.
//

import UIKit
import RealmSwift

class AllPostViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var realmObjcs: [RealmUser] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let xib = UINib(nibName: "AllPostCell", bundle: nil)
        tableView.register(xib, forCellReuseIdentifier: "AllPostCell")
        tableView.dataSource = self
        tableView.delegate = self
        
        realmObjcs = RealmManager.sharedInstatnce.fetchData()

        tableView.reloadData()
        
    }
    
}

extension AllPostViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return realmObjcs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "AllPostCell", for: indexPath) as! AllPostCell
        cell.update(realm: realmObjcs[indexPath.row])
        return cell
    }
    
    //    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
    //        return 83
    //    }
}
