//
//  AddPostViewController.swift
//  RealmProj
//
//  Created by Yaroslav Georgievich on 16.07.2019.
//  Copyright © 2019 Pavliuk. All rights reserved.
//

import UIKit

class AddPostViewController: UIViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var sexTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    
    var realmObjcs = RealmUser()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func addUser() {
        if let name = nameTextField.text {
            realmObjcs.name = name
        }
        if let surname = surnameTextField.text {
            realmObjcs.surname = surname
        }
        if let age = ageTextField.text {
            realmObjcs.age = Int(age) ?? 0
        }
        if let sex = sexTextField.text {
            realmObjcs.sex = sex
        }
        if let city = cityTextField.text {
            realmObjcs.cityOfBirthday = city
        }
        // AddUser with model
        RealmManager.sharedInstatnce.setData(model: realmObjcs)
    }
    
    @IBAction func addUserButton(_ sender: Any) {
        addUser()
        
        //переход на AllPostVC
        let allPostsVC = storyboard?.instantiateViewController(withIdentifier: "AllPostViewController") as! AllPostViewController
//        self.navigationController?.pushViewController(allPostsVC, animated: true)
        self.present(allPostsVC, animated: true)
    }
    
    
}
